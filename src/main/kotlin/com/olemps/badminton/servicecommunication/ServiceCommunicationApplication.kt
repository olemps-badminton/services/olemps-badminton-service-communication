package com.olemps.badminton.servicecommunication

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ServiceCommunicationApplication

fun main(args: Array<String>) {
	runApplication<ServiceCommunicationApplication>(*args)
}
